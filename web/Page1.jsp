<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Document   : Page1
    Created on : Sep 9, 2010, 1:32:40 PM
    Author     : jyeary
-->
<jsp:root version="2.1" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:webuijsf="http://www.sun.com/webui/webuijsf">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>
        <webuijsf:page id="page1">
            <webuijsf:html id="html1">
                <webuijsf:head id="head1">
                    <webuijsf:link id="link1" url="/resources/stylesheet.css"/>
                </webuijsf:head>
                <webuijsf:body id="body1" style="-rave-layout: grid">
                    <webuijsf:form id="form1">
                        <webuijsf:upload binding="#{Page1.fileUpload1}" id="fileUpload1" style="left: 24px; top: 24px; position: absolute"/>
                        <webuijsf:button actionExpression="#{Page1.uploadFileButton_action}" binding="#{Page1.uploadFileButton}" id="uploadFileButton"
                            primary="true" style="left: 23px; top: 72px; position: absolute" text="Upload File"/>
                        <webuijsf:staticText binding="#{Page1.fileNameStaticText}" id="fileNameStaticText" style="left: 96px; top: 120px; position: absolute"/>
                        <webuijsf:staticText binding="#{Page1.fileTypeStaticText}" id="fileTypeStaticText" style="left: 96px; top: 144px; position: absolute"/>
                        <webuijsf:staticText binding="#{Page1.fileSizeStaticText}" id="fileSizeStaticText" style="left: 96px; top: 168px; position: absolute"/>
                        <webuijsf:label id="fileNameLabel" style="left: 24px; top: 120px; position: absolute" text="File Name:"/>
                        <webuijsf:label id="fileTypeLabel" style="left: 24px; top: 144px; position: absolute" text="File Type:"/>
                        <webuijsf:label id="fileSizeLabel" style="left: 24px; top: 168px; position: absolute" text="File Size:"/>
                        <webuijsf:image binding="#{Page1.image1}" id="image1" style="left: 24px; top: 192px; position: absolute"/>
                        <webuijsf:messageGroup id="messageGroup1" style="left: 24px; top: 264px; position: absolute"/>
                        <webuijsf:button actionExpression="#{Page1.button1_action}" id="button1" style="left: 95px; top: 72px; position: absolute" text="Back"/>
                    </webuijsf:form>
                </webuijsf:body>
            </webuijsf:html>
        </webuijsf:page>
    </f:view>
</jsp:root>
