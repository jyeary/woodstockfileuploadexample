<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Document   : Page2
    Created on : Sep 9, 2010, 3:08:17 PM
    Author     : jyeary
-->
<jsp:root version="2.1" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:webuijsf="http://www.sun.com/webui/webuijsf">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>
        <webuijsf:script>
            document.getElementById('form1:fileChooser1').setChooseButton('form1:fileChooseButton');
        </webuijsf:script>
        <webuijsf:page id="page1">
            <webuijsf:html id="html1">
                <webuijsf:head id="head1">
                    <webuijsf:link id="link1" url="/resources/stylesheet.css"/>
                </webuijsf:head>
                <webuijsf:body id="body1" style="-rave-layout: grid">
                    <webuijsf:form enctype="multipart/form-data" id="form1">
                        <webuijsf:fileChooser binding="#{Page2.fileChooser1}" id="fileChooser1" multiple="true" selected="#{Page2.selectedFiles}" style="left: 24px; top: 24px; position: absolute"/>
                        <webuijsf:button actionExpression="#{Page2.fileChooseButton_action}" actionListenerExpression="#{Page2.chooseFile}"
                            id="fileChooseButton" primary="true" style="left: 23px; top: 480px; position: absolute" text="Choose File"/>
                        <webuijsf:messageGroup id="messageGroup1" style="position: absolute; left: 24px; top: 528px"/>
                        <webuijsf:button actionExpression="#{Page2.button1_action}" id="button1" style="left: 119px; top: 480px; position: absolute" text="Back"/>
                    </webuijsf:form>
                </webuijsf:body>
            </webuijsf:html>
        </webuijsf:page>
    </f:view>
</jsp:root>
