<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Document   : Index
    Created on : Sep 9, 2010, 3:04:40 PM
    Author     : jyeary
-->
<jsp:root version="2.1" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:webuijsf="http://www.sun.com/webui/webuijsf">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>
        <webuijsf:page id="page1">
            <webuijsf:html id="html1">
                <webuijsf:head id="head1">
                    <webuijsf:link id="link1" url="/resources/stylesheet.css"/>
                </webuijsf:head>
                <webuijsf:body id="body1" style="-rave-layout: grid">
                    <webuijsf:form id="form1">
                        <webuijsf:hyperlink id="hyperlink1" style="left: 24px; top: 24px; position: absolute" text="Example#1 - Upload Component" url="/faces/Page1.jsp"/>
                        <webuijsf:hyperlink id="hyperlink2" style="left: 24px; top: 48px; position: absolute" text="Example#2 - File Chooser Component" url="/faces/Page2.jsp"/>
                    </webuijsf:form>
                </webuijsf:body>
            </webuijsf:html>
        </webuijsf:page>
    </f:view>
</jsp:root>
